package parallel

import (
	"runtime"
	"sync"
)

func Start(numThreads int, callback func(thread, total int)) {
	if numThreads <= 0 {
		numThreads = runtime.NumCPU()
	}
	var wg sync.WaitGroup
	wg.Add(numThreads)
	for i := 0; i < numThreads; i++ {
		go func(i, numThreads int) {
			callback(i, numThreads)
			wg.Done()
		}(i, numThreads)
	}
	wg.Wait()
}
