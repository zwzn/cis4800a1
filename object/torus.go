package object

import (
	"math"
)

func NewTorus(sections int, hole float64) *Mesh {
	var m *Mesh
	m = newMesh()
	dps := math.Pi * 2 / float64(sections)
	for thata := 0.0; thata < math.Pi*2; thata += dps {
		for phi := 0.0; phi < math.Pi*2; phi += dps {
			var p1, p2, p3, p4, n1, n2, n3, n4 *Vector3
			p1 = tsToPoint(hole, phi, thata)
			p2 = tsToPoint(hole, phi, thata+dps)
			p3 = tsToPoint(hole, phi+dps, thata)
			p4 = tsToPoint(hole, phi+dps, thata+dps)

			n1 = NewDirectionVector3(phi, thata)
			n2 = NewDirectionVector3(phi, thata+dps)
			n3 = NewDirectionVector3(phi+dps, thata)
			n4 = NewDirectionVector3(phi+dps, thata+dps)

			m.addTriangle(NewTriangle(p1, p2, p3, n1, n2, n3))
			m.addTriangle(NewTriangle(p2, p3, p4, n2, n3, n4))
		}
	}
	return m
}

func tsToPoint(hole, phi, theta float64) *Vector3 {
	r := (2.0 - hole) / 4.0
	R := 1 - r

	return Vec3(
		(R+r*math.Cos(theta))*math.Cos(phi),
		(R+r*math.Cos(theta))*math.Sin(phi),
		r*math.Sin(theta),
	)
}
