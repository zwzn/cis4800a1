package object

import (
	"bitbucket.org/zwzn/cis4800a1/matrix"
)

type Object struct {
	meshes          []*Mesh
	transformations []*matrix.Matrix
}

func NewObject() *Object {
	return &Object{}
}

func (o *Object) GetFaces() []*Triangle {
	var faces []*Triangle
	for _, mesh := range o.meshes {
		faces = append(faces, mesh.GetFaces()...)
	}
	return faces
}

func (o *Object) AddMesh(m *Mesh) {
	o.meshes = append(o.meshes, m)
}

func (o *Object) AddCamera(m *matrix.Matrix) {
	o.transformations = append(o.transformations, m)
}

func (o *Object) GetCamera(i int) *matrix.Matrix {
	if i < 0 || i > len(o.transformations)-1 {
		return matrix.Identity(4)
	}
	return o.transformations[i]
}
