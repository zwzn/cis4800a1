package object

import "math"

func NewTube(sections int, hole float64) *Mesh {
	// TODO split vertically into more triangles
	var m *Mesh
	m = newMesh()
	dps := math.Pi * 2 / float64(sections)

	nTop := Vec3(0, 1, 0)
	nBottom := Vec3(0, -1, 0)
	hSize := hole / 2.0
	for angle := 0.0; angle < math.Pi*2; angle += dps {
		var p1, p2, p3, p4 *Vector3

		p1 = angleToPoint(angle, 1).Add(Vec3(0, 1, 0))
		p2 = angleToPoint(angle+dps, 1).Add(Vec3(0, 1, 0))
		p3 = angleToPoint(angle, hSize).Add(Vec3(0, 1, 0))
		p4 = angleToPoint(angle+dps, hSize).Add(Vec3(0, 1, 0))

		m.addTriangle(NewTriangle(p1, p2, p3, nTop, nTop, nTop))
		m.addTriangle(NewTriangle(p4, p2, p3, nTop, nTop, nTop))

		p1 = angleToPoint(angle, 1).Add(Vec3(0, -1, 0))
		p2 = angleToPoint(angle+dps, 1).Add(Vec3(0, -1, 0))
		p3 = angleToPoint(angle, hSize).Add(Vec3(0, -1, 0))
		p4 = angleToPoint(angle+dps, hSize).Add(Vec3(0, -1, 0))

		m.addTriangle(NewTriangle(p1, p2, p3, nBottom, nBottom, nBottom))
		m.addTriangle(NewTriangle(p4, p2, p3, nBottom, nBottom, nBottom))
		// m.addTriangle(NewTriangle(pBottom, p3, p4, nBottom, nBottom, nBottom))

		n1 := NewDirectionVector3(angle, 0)
		n2 := NewDirectionVector3(angle+dps, 0)
		pps := 2 / float64(sections)
		for i := -1.0; i < 1; i += pps {
			p1 = angleToPoint(angle, 1).Add(Vec3(0, i, 0))
			p2 = angleToPoint(angle+dps, 1).Add(Vec3(0, i+pps, 0))
			p3 = angleToPoint(angle, 1).Add(Vec3(0, i+pps, 0))
			p4 = angleToPoint(angle+dps, 1).Add(Vec3(0, i, 0))

			m.addTriangle(NewTriangle(p1, p2, p3, n1, n2, n1))
			m.addTriangle(NewTriangle(p2, p3, p4, n2, n1, n2))

			p1 = angleToPoint(angle, hSize).Add(Vec3(0, i, 0))
			p2 = angleToPoint(angle+dps, hSize).Add(Vec3(0, i+pps, 0))
			p3 = angleToPoint(angle, hSize).Add(Vec3(0, i+pps, 0))
			p4 = angleToPoint(angle+dps, hSize).Add(Vec3(0, i, 0))

			m.addTriangle(NewTriangle(p1, p2, p3, n1, n2, n1))
			m.addTriangle(NewTriangle(p2, p3, p4, n2, n1, n2))
		}
	}
	return m
}
