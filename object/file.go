package object

import (
	"encoding/xml"
	"fmt"
	"image/color"
	"io/ioutil"
	"math"

	"bitbucket.org/zwzn/cis4800a1/matrix"

	colors "gopkg.in/go-playground/colors.v1"
)

func FromFile(file string) (*Object, error) {
	type T struct {
		Type string  `xml:"type,attr"`
		X    float64 `xml:"x,attr"`
		Y    float64 `xml:"y,attr"`
		Z    float64 `xml:"z,attr"`
	}
	type A struct {
		Type  string  `xml:"type,attr"`
		Pitch float64 `xml:"pitch,attr"`
		Yaw   float64 `xml:"yaw,attr"`
		Roll  float64 `xml:"roll,attr"`
	}
	type C struct {
		Name     string `xml:"name,attr"`
		Position T      `xml:"position"`
		Angle    A      `xml:"angle"`
	}
	type M struct {
		Shape           string  `xml:"shape,attr"`
		Faces           int     `xml:"resolution,attr"`
		Hole            float64 `xml:"hole,attr"`
		Transformations []T     `xml:"transform"`
		Colour          string  `xml:"colour"`
	}
	type O struct {
		Meshes  []M `xml:"mesh"`
		Cameras []C `xml:"camera"`
	}
	v := O{}
	dat, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	// fmt.Println(string(dat))
	err = xml.Unmarshal(dat, &v)
	if err != nil {
		return nil, err
	}

	obj := NewObject()

	for _, cam := range v.Cameras {
		fmt.Printf("Camera %v\n", cam)

		dtor := math.Pi * 2 / 360

		m := matrix.Identity(4)

		m, err := matrix.Translate(
			-cam.Position.X,
			-cam.Position.Y,
			-cam.Position.Z).Multiply(m)
		if err != nil {
			return nil, err
		}
		m, err = matrix.Rotate(
			-cam.Angle.Pitch*dtor,
			-cam.Angle.Yaw*dtor,
			-cam.Angle.Roll*dtor).Multiply(m)
		if err != nil {
			return nil, err
		}

		fmt.Printf("\nMatrix:\n%v\n\n", m)
		obj.AddCamera(m)
	}

	for _, mesh := range v.Meshes {
		m := newMesh()
		if mesh.Shape == "sphere" {
			m = NewSphere(mesh.Faces)
		} else if mesh.Shape == "cube" {
			m = NewCube(mesh.Faces)
		} else if mesh.Shape == "cylinder" {
			m = NewCylinder(mesh.Faces)
		} else if mesh.Shape == "tube" {
			m = NewTube(mesh.Faces, mesh.Hole)
		} else if mesh.Shape == "torus" {
			m = NewTorus(mesh.Faces, mesh.Hole)
		}
		// fmt.Printf("%v\n", mesh.Transformations)
		for _, tr := range mesh.Transformations {
			if tr.Type == "translate" {
				err := m.Transform(matrix.Translate(tr.X, tr.Y, tr.Z))
				if err != nil {
					return nil, err
				}
			} else if tr.Type == "scale" {
				err := m.Transform(matrix.Scale(tr.X, tr.Y, tr.Z))
				if err != nil {
					return nil, err
				}
			} else if tr.Type == "rotate" {
				dtor := math.Pi * 2 / 360
				err := m.Transform(matrix.Rotate(tr.X*dtor, tr.Y*dtor, tr.Z*dtor))
				if err != nil {
					return nil, err
				}
			}
		}

		hex, err := colors.ParseHEX(mesh.Colour)
		if err != nil {
			return nil, err
		}

		rgba := hex.ToRGBA()
		m.SetColour(color.RGBA{uint8(rgba.R), uint8(rgba.G), uint8(rgba.B), uint8(rgba.A * 255)})
		obj.AddMesh(m)
	}
	return obj, nil
}
