package renderer

import (
	"fmt"
	"image"
	"image/color"
	"math"

	"bitbucket.org/zwzn/cis4800a1/object"
	"bitbucket.org/zwzn/cis4800a1/parallel"
)

// func RenderObject2(o *object.Object, width int, height int) *image.RGBA {
// 	r := image.Rect(0, 0, width, height)
// 	img := image.NewRGBA(r)
// 	faces := o.GetFaces()

// 	camera := object.Vec3(0, 0, -1)

// 	threads := runtime.NumCPU()

// 	var wg sync.WaitGroup
// 	wg.Add(threads)
// 	for i := 0; i < threads; i++ {
// 		go func(num int, total int) {
// 			for x := width / total * num; x < width/total*(num+1); x++ {
// 				if num == 0 {
// 					loading(x+1, width/total*(num+1), 40)
// 				}
// 				for y := 0; y < height; y++ {
// 					drawP := object.Vec2(float64(x), float64(y))
// 					toDraw := []*object.Triangle{}
// 					for _, face := range faces {
// 						pa, pb, pc := project(face.A, width, height),
// 							project(face.B, width, height),
// 							project(face.C, width, height)

// 						if pointInTriangle(drawP, pa, pb, pc) {
// 							toDraw = append(toDraw, face)
// 						}

// 					}

// 					if len(toDraw) >= 1 {
// 						var min *object.Triangle
// 						for _, face := range toDraw {
// 							if min == nil || face.Distance(camera) < min.Distance(camera) {
// 								min = face
// 							}
// 						}
// 						n := min.Normal(unproject(min, drawP, width, height)).ToUnit()
// 						l := object.Vec3(1, 1, 1)
// 						dif := 1 - l.Angle(n)/math.Pi
// 						// fmt.Printf("l %v n %v dif %.2f\n", l, n, dif)

// 						//dif = unproject(min, object.NewVector2(float64(x), float64(y)), width, height).Z / 2
// 						// fmt.Printf("%v %p\n", n, min)
// 						img.SetRGBA(
// 							x,
// 							y,
// 							// min.Colour,
// 							color.RGBA{uint8(dif * float64(min.Colour.R)), uint8(dif * float64(min.Colour.G)), uint8(dif * float64(min.Colour.B)), 255},
// 							// color.RGBA{uint8(n.X * 255), uint8(n.Y * 255), uint8(n.Z * 255), 255},
// 						)
// 						// break
// 					} else {

// 						img.SetRGBA(
// 							x,
// 							y,
// 							color.RGBA{255, 255, 255, 255},
// 						)
// 					}
// 				}
// 			}
// 			wg.Done()
// 		}(i, threads)
// 	}
// 	wg.Wait()

// 	fmt.Println()
// 	// for _, face := range faces {
// 	// 	drawLine(img,
// 	// 		project(face.A, width, height),
// 	// 		project(face.B, width, height))
// 	// 	drawLine(img,
// 	// 		project(face.C, width, height),
// 	// 		project(face.B, width, height))
// 	// 	drawLine(img,
// 	// 		project(face.A, width, height),
// 	// 		project(face.C, width, height))
// 	// }

// 	return img
// }

func RenderObject3(o *object.Object, width int, height int) *image.RGBA {
	r := image.Rect(0, 0, width, height)
	img := image.NewRGBA(r)
	faces := o.GetFaces()
	depth := make([][]float64, width)
	for i := range depth {
		depth[i] = make([]float64, height)
	}
	// for x := 0; x < width; x++ {
	// 	for y := 0; y < height; y++ {
	// 		img.SetRGBA(
	// 			x,
	// 			y,
	// 			color.RGBA{255, 255, 255, 255},
	// 		)
	// 	}
	// }

	parallel.Start(1, func(thread, total int) {
		max := len(faces)
		for i := int(math.Floor(float64(max) / float64(total) * float64(thread))); i < int(math.Ceil(float64(max)/float64(total)*float64(thread+1))); i++ {
			face := faces[i]
			if thread == 0 {
				loading(i+1, max/total, 30)
			}

			a := project(face.A, width, height)
			b := project(face.B, width, height)
			c := project(face.C, width, height)

			minX, maxX := math.Min(a.X, math.Min(b.X, c.X)), math.Max(a.X, math.Max(b.X, c.X))
			minY, maxY := math.Min(a.Y, math.Min(b.Y, c.Y)), math.Max(a.Y, math.Max(b.Y, c.Y))

			// depthA := face.A.Z + 1
			// depthB := face.B.Z + 1
			// depthC := face.C.Z + 1

			// fmt.Printf("\n%v %v %v\n%v\n", face.A, face.B, face.C, object.Vec3(depthA, depthB, depthC))
			for x := math.Floor(minX); x <= math.Ceil(maxX); x++ {
				for y := math.Floor(minY); y <= math.Ceil(maxY); y++ {
					point := object.Vec2(x, y)
					if pointInTriangle(point, a, b, c) {
						// da := point.Distance(a)
						// db := point.Distance(b)
						// dc := point.Distance(c)

						// // weighted average
						// wa := ((db * dc) / (da*db + da*dc + db*dc))
						// wb := ((da * dc) / (da*db + da*dc + db*dc))
						// wc := ((da * db) / (da*db + da*dc + db*dc))

						// // linear average
						// la := (db + dc) / (a.Distance(b) + a.Distance(c))
						// lb := (db + dc) / (b.Distance(a) + b.Distance(c))
						// lc := (da + db) / (c.Distance(a) + c.Distance(b))

						// d := wa*depthA + wb*depthB + wc*depthC

						d := unproject(face, point, width, height).Z + 1

						if x > 0 && x < float64(width) && y > 0 && y < float64(height) &&
							(d > 0 && (d < depth[int(x)][int(y)] || depth[int(x)][int(y)] == 0)) {
							depth[int(x)][int(y)] = d
							img.SetRGBA(
								int(x),
								int(y),
								// color.RGBA{0, 0, 0, uint8(255 * (d / 3))},
								color.RGBA{0, uint8(255 * (d / 3)), uint8(255 - 255*(d/3)), 255},
								// color.RGBA{uint8(255 * wa), uint8(255 * wb), uint8(255 * wc), 255},
							)
						}
					}
				}
			}

		}
		fmt.Println()
	})

	// for y := 0; y < height; y++ {
	// 	for x := 0; x < width; x++ {
	// 		fmt.Printf("%.2f", depth[x][y])
	// 		if x < width-1 {
	// 			fmt.Printf("\t")
	// 		}
	// 	}
	// 	fmt.Println()
	// }

	// for _, face := range faces {
	// 	drawLine(img,
	// 		project(face.A, width, height),
	// 		project(face.B, width, height))
	// 	drawLine(img,
	// 		project(face.C, width, height),
	// 		project(face.B, width, height))
	// 	drawLine(img,
	// 		project(face.A, width, height),
	// 		project(face.C, width, height))
	// }

	fmt.Println()
	return img
}

func unproject(t *object.Triangle, p2 *object.Vector2, width int, height int) *object.Vector3 {
	// return object.Vec3(0, 0, (t.A.Z+t.B.Z+t.C.Z)/3)
	w := float64(width - 1)
	h := float64(height - 1)

	p := object.Vec2(p2.X, -p2.Y).
		Dot(2 / math.Min(w, h)).
		Subtract(object.Vec2(1, 1))

	v3 := object.Vec3(p.X, p.Y, -1)

	// return v3
	oldDist := float64(10000)
	for {
		v3.Z += 0.01

		newDist := v3.Distance(t.A) + v3.Distance(t.B) + v3.Distance(t.C)
		if newDist > oldDist {
			return v3
		}
		oldDist = newDist
	}
}
