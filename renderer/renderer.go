package renderer

import (
	"fmt"
	"image"
	"image/color"
	"log"
	"math"

	"bitbucket.org/zwzn/cis4800a1/object"
	"bitbucket.org/zwzn/cis4800a1/parallel"
)

func RenderObject(o *object.Object, width int, height int) (*image.RGBA, error) {
	r := image.Rect(0, 0, width, height)
	img := image.NewRGBA(r)
	faces := o.GetFaces()

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			img.SetRGBA(
				x,
				y,
				color.RGBA{255, 255, 255, 255},
			)
		}
	}

	cam := o.GetCamera(0)
	fmt.Printf("%v\n", cam)
	parallel.Start(-1, func(thread, total int) {
		max := len(faces)
		for i := int(math.Floor(float64(max) / float64(total) * float64(thread))); i < int(math.Ceil(float64(max)/float64(total)*float64(thread+1))); i++ {
			face := faces[i]
			if thread == 0 {
				loading(i+1, max/total, 40)
			}
			t, err := cam.Multiply(face.Mesh.Transformation)
			if err != nil {
				log.Fatalf("error in render: %v\n", err)
			}

			a, err := face.A.Transform(t)
			if err != nil {
				log.Fatalf("error in render: %v\n", err)
			}
			b, err := face.B.Transform(t)
			if err != nil {
				log.Fatalf("error in render: %v\n", err)
			}
			c, err := face.C.Transform(t)
			if err != nil {
				log.Fatalf("error in render: %v\n", err)
			}

			drawLine(img,
				project(a, width, height),
				project(b, width, height))
			drawLine(img,
				project(c, width, height),
				project(b, width, height))
			drawLine(img,
				project(a, width, height),
				project(c, width, height))
		}
	})

	fmt.Println()
	return img, nil
}

//start from https://stackoverflow.com/a/2049593
func sign(p1, p2, p3 *object.Vector2) float64 {
	return (p1.X-p3.X)*(p2.Y-p3.Y) - (p2.X-p3.X)*(p1.Y-p3.Y)
}

func pointInTriangle(vt, v1, v2, v3 *object.Vector2) bool {
	var b1, b2, b3 bool

	b1 = sign(vt, v1, v2) < 0.0
	b2 = sign(vt, v2, v3) < 0.0
	b3 = sign(vt, v3, v1) < 0.0

	return ((b1 == b2) && (b2 == b3))
}

// end from https://stackoverflow.com/a/2049593

var lastPer = -1

func loading(num, total, width int) {
	per := int(float32(num) / float32(total) * 100)
	if per != lastPer {
		fmt.Printf("\r%7s |", fmt.Sprintf("%d/%d", num, total))
		for i := 0; i < width; i++ {
			if float32(num)/float32(total) >= float32(i)/float32(width) {
				fmt.Print("=")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Printf("|%d%%", per)
	}
}

func project(p *object.Vector3, width int, height int) *object.Vector2 {
	w := float64(width - 1)
	h := float64(height - 1)

	newP := object.Vec2(p.X, -p.Y).
		Add(object.Vec2(1, 1)).
		Dot(math.Min(w, h) / 2)
	return newP
}

func drawLine(img *image.RGBA, p1 *object.Vector2, p2 *object.Vector2) {
	var sx, ex, sy, ey float64
	if math.Abs(p1.X-p2.X) > math.Abs(p1.Y-p2.Y) {
		if p1.X < p2.X {
			sx = p1.X
			sy = p1.Y
			ex = p2.X
			ey = p2.Y
		} else {
			sx = p2.X
			sy = p2.Y
			ex = p1.X
			ey = p1.Y
		}

		// fmt.Println("x", sx, sy, ex, ey)
		for x := sx; x <= ex; x++ {
			y := sy + (x-sx)/(sx-ex)*(sy-ey)
			img.SetRGBA(
				int(x),
				int(y),
				color.RGBA{0, 0, 0, 255},
			)
		}
	} else {
		if p1.Y < p2.Y {
			sx = p1.X
			sy = p1.Y
			ex = p2.X
			ey = p2.Y
		} else {
			sx = p2.X
			sy = p2.Y
			ex = p1.X
			ey = p1.Y
		}
		// fmt.Println("y", sx, sy, ex, ey)
		for y := sy + 1; y <= ey; y++ {
			x := sx + (y-sy)/(sy-ey)*(sx-ex)
			// fmt.Println(x, y)
			img.SetRGBA(
				int(x),
				int(y),
				color.RGBA{0, 0, 0, 255},
			)
		}
	}
}

func drawPoint(img *image.RGBA, p *object.Vector2) {
	img.SetRGBA(
		int(p.X),
		int(p.Y),
		color.RGBA{0, 0, 0, 255},
	)
}
