package matrix

import (
	"fmt"
	"math"
	"strings"
)

type Matrix struct {
	data   [][]float64
	width  int
	height int
}

func NewMatrix(width int, height int, vals ...float64) *Matrix {

	var d [][]float64
	d = make([][]float64, width)
	for i, _ := range d {
		d[i] = make([]float64, height)
	}

	for i, val := range vals {
		x := i % width
		y := int(math.Floor(float64(i) / float64(width)))

		if x < width && y < height {
			d[x][y] = val
		}
	}

	return &Matrix{d, width, height}
}

func Mat4(
	aa, ab, ac, ad,
	ba, bb, bc, bd,
	ca, cb, cc, cd,
	da, db, dc, dd float64) *Matrix {

	m := NewMatrix(4, 4)
	m.data[0][0], m.data[1][0], m.data[2][0], m.data[3][0] = aa, ab, ac, ad
	m.data[0][1], m.data[1][1], m.data[2][1], m.data[3][1] = ba, bb, bc, bd
	m.data[0][2], m.data[1][2], m.data[2][2], m.data[3][2] = ca, cb, cc, cd
	m.data[0][3], m.data[1][3], m.data[2][3], m.data[3][3] = da, db, dc, dd

	return m
}

func Identity(size int) *Matrix {
	m := NewMatrix(size, size)
	for x := 0; x < size; x++ {
		for y := 0; y < size; y++ {
			if x == y {
				m.data[x][y] = 1
			} else {
				m.data[x][y] = 0
			}
		}
	}
	return m
}

func (a *Matrix) Multiply(b *Matrix) (*Matrix, error) {
	if a == nil || b == nil {
		return nil, fmt.Errorf("One of the input matrices is nil")
	}
	if a.height != b.width {
		return nil, fmt.Errorf("matrix.Multiply: matrix sizes don't match, a[%d, %d] b[%d, %d]",
			a.width, a.height,
			b.width, b.height)
	}
	n := a.width
	m := a.height
	p := b.height

	// fmt.Printf("\nNew %dx%d matrix\n", n, p)
	c := NewMatrix(n, p)
	for i := 0; i < n; i++ {
		for j := 0; j < p; j++ {
			sum := 0.0
			for k := 0; k < m; k++ {
				// fmt.Printf("%.1f*%.1f + ", a.Get(i, k), b.Get(k, j))
				sum += a.Get(i, k) * b.Get(k, j)

			}
			// print("\n")
			c.Set(i, j, sum)
		}
	}
	return c, nil
}

func (this *Matrix) Get(x, y int) float64 {
	// fmt.Printf("X %d < %d, Y %d < %d\n", x, this.width, y, this.height)
	return this.data[x][y]
}

func (this *Matrix) Set(x int, y int, val float64) {
	this.data[x][y] = val
}

func (this *Matrix) String() string {
	var str string
	for y := 0; y < this.height; y++ {
		temp := make([]string, this.width)
		for x := 0; x < this.width; x++ {
			temp[x] = fmt.Sprintf("%6.2f", this.Get(x, y))
		}

		str += "|" + strings.Join(temp, ", ") + "|\n"
	}
	return str[:len(str)-1]
}
