package main

import (
	"flag"
	"image"
	"image/png"
	"log"
	"os"

	"bitbucket.org/zwzn/cis4800a1/object"
	"bitbucket.org/zwzn/cis4800a1/renderer"
)

func check(e error) {
	if e != nil {
		log.Fatalf("%v\n", e)
	}
}

func main() {
	// m1 := matrix.Identity(4)
	// // m2, _ := m1.Multiply(matrix.Rotate(object.Vec3(1, 0, 0)))
	// m2, _ := matrix.Scale(object.Vec3(2, 2, 2)).Multiply(m1)
	// m2, _ = matrix.Translate(object.Vec3(1, 2, 3)).Multiply(m2)

	// m3 := matrix.NewMatrix(4, 1,
	// 	1, 1, 1, 1)

	// m4, _ := m2.Multiply(m3)
	// fmt.Printf("%v\n\n%v\n\n%v\n\n%v\n", m1, m2, m3, m4)
	// return
	inFile := flag.String("if", "", "an xobj file to be rendered")
	outFile := flag.String("of", "image.png", "the image file to output")
	rend := flag.String("renderer", "a1", "The renderer to use")
	size := flag.Int("size", 1000, "The size of the output image")
	flag.Parse()

	// v := object.NewVector3(1, 2, 3)
	// fmt.Println(v)
	// fmt.Println(v.ToBasis(object.IdentityBasis))

	if *inFile == "" {
		log.Fatalf("You must enter an xobj file to be rendered\n")
	}
	// num := int(math.Sqrt(1000) / 2)
	// o := object.NewCylinder(50)

	obj, err := object.FromFile(*inFile)
	check(err)
	var img image.Image
	if *rend == "adam" {
		//img = renderer.RenderObject2(obj, *size, *size)
		log.Fatal("I removed this")
	} else if *rend == "adam2" {
		img = renderer.RenderObject3(obj, *size, *size)
	} else if *rend == "a1" {
		img, err = renderer.RenderObject(obj, *size, *size)
		check(err)
	} else {
		img, err = renderer.RenderObject(obj, *size, *size)
		check(err)
	}
	saveImage(img, *outFile)
}

func saveImage(img image.Image, file string) {

	f, err := os.Create(file)
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
